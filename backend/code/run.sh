#/bin/sh
pipenv install

pipenv run gunicorn -w 1 -b 0.0.0.0:8080 --chdir /code/app --reload "main:run_app()"
