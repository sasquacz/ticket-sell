#/bin/sh
pipenv install --dev

(cd app && pipenv run pylint_runner)

cd app && pipenv run python -m pytest
