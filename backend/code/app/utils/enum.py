import enum

class BookingStatus(enum.Enum):
    waiting_for_payment = 1
    cancelled = 2
    payed = 3

class TicketCurrency(enum.Enum):
    EUR = 1
    USD = 2
