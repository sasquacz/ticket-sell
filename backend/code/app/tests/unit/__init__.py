import uuid

from datetime import datetime
from falcon import testing

import main
from database.manager import DBManager
from database.query import QueryManager
from utils.enum import BookingStatus

class BaseTestCase(testing.TestCase):
    def setUp(self):
        super(BaseTestCase, self).setUp()
        self.db_manager = main.initialize_db('sqlite:///:memory:')
        self.query = QueryManager(self.db_manager.session)
        self.uuid = str(uuid.uuid4())

        self.app = main.create_app(self.db_manager)

    def celery_cancel_booking_mock(self, *_, **kwargs):
        booking_id, ticket_id, no_tickets, = kwargs['args']
        ticket = self.query.get_ticket(ticket_id)
        booking = self.query.get_booking(booking_id)

        booking.status = BookingStatus.cancelled
        ticket.available += no_tickets

        self.query.save_all([booking, ticket])

        return self.uuid

    def celery_pay_booking_mock(self, *_, **kwargs):
        booking_id = kwargs['args']
        booking = self.query.get_booking(booking_id)

        booking.status = BookingStatus.payed
        booking.payment_time = datetime.utcfromtimestamp(1565128634)

        self.query.save(booking)

        return self.uuid
