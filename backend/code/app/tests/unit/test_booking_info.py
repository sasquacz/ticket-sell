from datetime import datetime
from unittest import mock

from database import models
from tests.unit import BaseTestCase
from utils.enum import BookingStatus


class BookingInfoTestCase(BaseTestCase):
    def setUp(self):
        super(BookingInfoTestCase, self).setUp()
        ev1 = models.Event("Eventi1", datetime.utcfromtimestamp(1565128534))
        ti1 = models.Ticket("Normal", 1, 100, 10)
        self.query.save_all([ev1, ti1])

    @mock.patch('celery.Celery')
    def test_get_ok(self, celery_mock):
        """
            Test /booking_info
            Normal scenario with all steps successful:
                Ticket booking
                Successful payment
                Info about booking
        """

        id_ = 1
        no_tickets = 2

        celery_mock().__enter__().send_task.return_value = self.uuid
        self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")
        celery_mock().__enter__().send_task.side_effect = self.celery_pay_booking_mock
        celery_mock().__enter__().control().revoke.return_value = True
        self.simulate_get(f"/tickets_payment?token={self.uuid}")
        booking = self.query.get_booking_by_token(self.uuid)
        result = self.simulate_get(f"/booking_info?id={booking.id}")

        expected_json = {'cost': 20, 'currency': 'EUR', 'id': booking.id, 'no_tickets': no_tickets, 'status': BookingStatus.payed.name, 'ticket_id': id_, 'token': self.uuid}

        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.json, expected_json)

    def test_get_fail_not_found_id(self):
        """
            Test /booking_info
            Scenario with id which is not stored in database and app
        """

        id_ = 99

        result = self.simulate_get(f"/booking_info?id={id_}")

        expected_json = {'description': 'Booking not found.'}

        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.json, expected_json)

    def test_get_fail_bad_type_id(self):
        """
            Test /booking_info
            Scenario with id of invalid type
        """

        id_ = "z"

        result = self.simulate_get(f"/booking_info?id={id_}")

        expected_json = {'title': 'Invalid parameter', 'description': 'The "id" parameter is invalid. The value must be an integer.'}

        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.json, expected_json)
