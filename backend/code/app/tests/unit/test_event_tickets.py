from datetime import datetime

from database import models
from tests.unit import BaseTestCase


class EventTicketsTestCase(BaseTestCase):
    def setUp(self):
        super(EventTicketsTestCase, self).setUp()
        ev1 = models.Event("Eventi1", datetime.utcfromtimestamp(1565128534))
        ti1 = models.Ticket("Normal", 1, 100, 10)
        ti2 = models.Ticket("VIP", 1, 10, 100)
        self.query.save_all([ev1, ti1, ti2])


    def test_get_ok(self):
        """
            Test /event_tickets
            Normal scenario with success access
        """

        id_ = 1

        result = self.simulate_get(f"/event_tickets?id={id_}")

        expected_json = [{'ticket_type': 'Normal', 'event_id': 1, 'total': 100, 'available': 100, 'price': 10, 'currency': 'EUR', 'can_buy': True}, {'ticket_type': 'VIP', 'event_id': 1, 'total': 10, 'available': 10, 'price': 100, 'currency': 'EUR', 'can_buy': True}]

        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.json, expected_json)

    def test_get_fail_not_found_id(self):
        """
            Test /event_tickets
            Scenario with id which is not stored in database and app
        """

        id_ = -10

        result = self.simulate_get(f"/event_tickets?id={id_}")

        expected_json = {"description": "Event not found."}

        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.json, expected_json)

    def test_get_fail_bad_type_id(self):
        """
            Test /event_tickets
            Scenario with id of invalid type
        """

        id_ = 'e'

        result = self.simulate_get(f"/event_tickets?id={id_}")

        expected_json = {'title': 'Invalid parameter', 'description': 'The "id" parameter is invalid. The value must be an integer.'}

        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.json, expected_json)
