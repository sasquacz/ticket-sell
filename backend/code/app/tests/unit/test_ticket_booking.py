from datetime import datetime
from unittest import mock

from database import models
from tests.unit import BaseTestCase
from utils.enum import BookingStatus


class TicketBookingTestCase(BaseTestCase):
    def setUp(self):
        super(TicketBookingTestCase, self).setUp()
        self.ev1 = models.Event("Eventi1", datetime.utcfromtimestamp(1565128534))
        self.ti1 = models.Ticket("Normal", 1, 100, 10)
        self.ti2 = models.Ticket("VIP", 1, 10, 101)
        self.query.save_all([self.ev1, self.ti1, self.ti2])

    @mock.patch('celery.Celery')
    def test_get_ok_booking_waiting_for_payment(self, celery_mock):
        """
            Test /ticket_booking
            Normal scenario with successful reservation. Booking is waiting for payment.
        """

        id_ = 1
        no_tickets = 2

        ticket_before = self.ti1.as_dict
        celery_mock().__enter__().send_task.return_value = self.uuid
        result = self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")
        ticket_after = self.ti1.as_dict
        booking = self.query.get_booking_by_token(self.uuid).as_dict

        expected_json = {'event_id': ticket_before['event_id'], 'ticket_id': id_, 'no_tickets': 2, 'total_cost': ticket_before['price'] * no_tickets, 'payment_token': self.uuid, 'status': booking['status']}

        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.json, expected_json)
        self.assertEqual(ticket_before['available'], ticket_after['available'] + no_tickets)
        self.assertEqual(BookingStatus.waiting_for_payment.name, booking['status'])

    @mock.patch('celery.Celery')
    def test_get_ok_booking_cancelled(self, celery_mock):
        """
            Test /ticket_booking
            Normal scenario with successful reservation, but payment time expired.
        """

        id_ = 1
        no_tickets = 2

        ticket_before = self.ti1.as_dict
        celery_mock().__enter__().send_task.side_effect = self.celery_cancel_booking_mock
        result = self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")
        ticket_after = self.ti1.as_dict
        booking = self.query.get_booking_by_token(self.uuid).as_dict

        expected_json = {'event_id': ticket_before['event_id'], 'ticket_id': id_, 'no_tickets': 2, 'total_cost': ticket_before['price'] * no_tickets, 'payment_token': self.uuid, 'status': booking['status']}

        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.json, expected_json)
        self.assertEqual(ticket_before['available'], ticket_after['available'])
        self.assertEqual(BookingStatus.cancelled.name, booking['status'])

    def test_get_fail_not_found_id(self):
        """
            Test /ticket_booking
            Scenario with id which is not stored in database and app
        """

        id_ = 9
        no_tickets = 1

        result = self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")

        expected_json = {'description': 'Tickets for this event not found.'}

        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.json, expected_json)

    def test_get_fail_bad_type_id(self):
        """
            Test /ticket_booking
            Scenario with id of invalid type
        """

        id_ = 'z'
        no_tickets = -41

        result = self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")

        expected_json = {'title': 'Invalid parameter', 'description': 'The "id" parameter is invalid. The value must be an integer.'}

        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.json, expected_json)

    def test_get_fail_invalid_no_ticket(self):
        """
            Test /ticket_booking
            Scenario with no_tickets from banned range
        """

        id_ = 1
        no_tickets = -41

        result = self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")

        expected_json = {'description': 'Cannot buy less than one ticket.'}

        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.json, expected_json)

    def test_get_fail_to_many_tickets(self):
        """
            Test /ticket_booking
            Scenario with no_tickets more than available
        """

        id_ = 1
        no_tickets = 500

        bookings_before = self.query.get_objects(models.Booking)
        ticket_before = self.ti1.as_dict
        result = self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")
        bookings_after = self.query.get_objects(models.Booking)
        ticket_after = self.ti1.as_dict

        expected_json = {'description': 'Cannot buy 500 tickets, but still is available 100'}

        self.assertEqual(result.status_code, 409)
        self.assertEqual(result.json, expected_json)
        self.assertEqual(ticket_before['available'], ticket_after['available'])
        self.assertEqual(len(bookings_before), len(bookings_after))
