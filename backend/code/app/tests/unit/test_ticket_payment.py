from datetime import datetime
from unittest import mock

from database import models
from tests.unit import BaseTestCase
from utils.enum import BookingStatus, TicketCurrency


class TicketPaymentTestCase(BaseTestCase):
    def setUp(self):
        super(TicketPaymentTestCase, self).setUp()
        self.ev1 = models.Event("Eventi1", datetime.utcfromtimestamp(1565128534))
        self.ti1 = models.Ticket("Normal", 1, 100, 10)
        self.ti2 = models.Ticket("VIP", 1, 10, 100)
        self.query.save_all([self.ev1, self.ti1, self.ti2])

    @mock.patch('celery.Celery')
    def test_get_ok_booking_payed(self, celery_mock):
        """
            Test /tickets_payment
            Normal scenario with all steps successful:
                Ticket booking
                Successful payment
        """

        id_ = 1
        no_tickets = 2

        ticket_before = self.ti1.as_dict
        celery_mock().__enter__().send_task.return_value = self.uuid
        self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")
        celery_mock().__enter__().send_task.side_effect = self.celery_pay_booking_mock
        celery_mock().__enter__().control().revoke.return_value = True
        result = self.simulate_get(f"/tickets_payment?token={self.uuid}")
        ticket_after = self.ti1.as_dict
        booking = self.query.get_booking_by_token(self.uuid).as_dict

        expected_json = {'description': 'Payment complete.'}

        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.json, expected_json)
        self.assertEqual(ticket_before['available'], ticket_after['available'] + no_tickets)
        self.assertEqual(BookingStatus.payed.name, booking['status'])

    @mock.patch('celery.Celery')
    def test_get_fail_not_valid_token(self, celery_mock):
        """
            Test /tickets_payment
            Scenario with rejected token by UUID validator
        """

        id_ = 1
        no_tickets = 2
        token = "00000000-0000-0000-0000-0000000000000" # one 0 too much

        celery_mock().__enter__().send_task.return_value = self.uuid
        self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")
        celery_mock().__enter__().send_task.side_effect = self.celery_pay_booking_mock
        celery_mock().__enter__().control().revoke.return_value = True
        result = self.simulate_get(f"/tickets_payment?token={token}")

        expected_json = {'description': 'The "token" parameter is invalid. The value must be a UUID string.', 'title': 'Invalid parameter'}

        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.json, expected_json)

    @mock.patch('celery.Celery')
    def test_get_fail_not_found_token(self, celery_mock):
        """
            Test /tickets_payment
            Scenario with token which is not stored in database and app
        """

        id_ = 1
        no_tickets = 2
        token = "00000000-0000-0000-0000-000000000000"

        celery_mock().__enter__().send_task.return_value = self.uuid
        self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")
        celery_mock().__enter__().send_task.side_effect = self.celery_pay_booking_mock
        celery_mock().__enter__().control().revoke.return_value = True
        result = self.simulate_get(f"/tickets_payment?token={token}")

        expected_json = {'description': 'Booking not found.'}

        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.json, expected_json)

    @mock.patch('celery.Celery')
    def test_get_fail_card_error(self, celery_mock):
        """
            Test /tickets_payment
            Scenario with token calling card error in PaymentGateway
        """

        id_ = 1
        no_tickets = 2
        token = "042800b6-9759-4235-a8f2-592d4ab0c2a1" # card error token

        celery_mock().__enter__().send_task.return_value = self.uuid
        self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")
        booking = self.query.get_booking_by_token(self.uuid)
        booking.token = token
        self.query.save(booking)
        celery_mock().__enter__().send_task.side_effect = self.celery_pay_booking_mock
        celery_mock().__enter__().control().revoke.return_value = True
        result = self.simulate_get(f"/tickets_payment?token={token}")

        expected_json = {'description': 'Your card has been declined'}

        self.assertEqual(result.status_code, 406)
        self.assertEqual(result.json, expected_json)

    @mock.patch('celery.Celery')
    def test_get_fail_payment_error(self, celery_mock):
        """
            Test /tickets_payment
            Scenario with token calling payment error in PaymentGateway
        """

        id_ = 1
        no_tickets = 3
        token = "2b88c420-3cb1-4df0-ba92-60d000217480" # payment error token

        celery_mock().__enter__().send_task.return_value = self.uuid
        self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")
        booking = self.query.get_booking_by_token(self.uuid)
        booking.token = token
        self.query.save(booking)
        celery_mock().__enter__().send_task.side_effect = self.celery_pay_booking_mock
        celery_mock().__enter__().control().revoke.return_value = True
        result = self.simulate_get(f"/tickets_payment?token={token}")

        expected_json = {'description': 'Something went wrong with your transaction'}

        self.assertEqual(result.status_code, 500)
        self.assertEqual(result.json, expected_json)

    @mock.patch('celery.Celery')
    def test_get_fail_currency_error(self, celery_mock):
        """
            Test /tickets_payment
            Scenario with token calling payment error in PaymentGateway
        """

        id_ = 1
        no_tickets = 1

        celery_mock().__enter__().send_task.return_value = self.uuid
        self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")
        booking = self.query.get_booking_by_token(self.uuid)
        booking.currency = TicketCurrency.USD
        self.query.save(booking)
        celery_mock().__enter__().send_task.side_effect = self.celery_pay_booking_mock
        celery_mock().__enter__().control().revoke.return_value = True
        result = self.simulate_get(f"/tickets_payment?token={self.uuid}")

        expected_json = {'description': 'Currency USD not supported'}

        self.assertEqual(result.status_code, 500)
        self.assertEqual(result.json, expected_json)

    @mock.patch('celery.Celery')
    def test_get_fail_payed_before(self, celery_mock):
        """
            Test /tickets_payment
            Scenario with payment already taken place
        """

        id_ = 1
        no_tickets = 2

        ticket_before = self.ti1.as_dict
        celery_mock().__enter__().send_task.return_value = self.uuid
        self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")
        celery_mock().__enter__().control().revoke.return_value = True
        celery_mock().__enter__().send_task.side_effect = self.celery_pay_booking_mock
        self.simulate_get(f"/tickets_payment?token={self.uuid}")
        result = self.simulate_get(f"/tickets_payment?token={self.uuid}")
        ticket_after = self.ti1.as_dict
        booking = self.query.get_booking_by_token(self.uuid).as_dict

        expected_json = {'description': 'Timeout, booking canceled, please reserve ticket again and complete payment in 15 minutes. Or Your payment completed earlier.'}

        self.assertEqual(result.status_code, 408)
        self.assertEqual(result.json, expected_json)
        self.assertEqual(ticket_before['available'], ticket_after['available'] + no_tickets)
        self.assertEqual(BookingStatus.payed.name, booking['status'])

    @mock.patch('celery.Celery')
    def test_get_fail_timeout(self, celery_mock):
        """
            Test /tickets_payment
            Scenario with booking timeout (15m)
        """

        id_ = 1
        no_tickets = 2

        ticket_before = self.ti1.as_dict
        celery_mock().__enter__().send_task.side_effect = self.celery_cancel_booking_mock
        self.simulate_get(f"/tickets_booking?id={id_}&no_tickets={no_tickets}")
        celery_mock().__enter__().send_task.side_effect = self.celery_pay_booking_mock
        celery_mock().__enter__().control().revoke.return_value = True
        result = self.simulate_get(f"/tickets_payment?token={self.uuid}")
        ticket_after = self.ti1.as_dict
        booking = self.query.get_booking_by_token(self.uuid).as_dict

        expected_json = {'description': 'Timeout, booking canceled, please reserve ticket again and complete payment in 15 minutes. Or Your payment completed earlier.'}

        self.assertEqual(result.status_code, 408)
        self.assertEqual(result.json, expected_json)
        self.assertEqual(ticket_before['available'], ticket_after['available'])
        self.assertEqual(BookingStatus.cancelled.name, booking['status'])
