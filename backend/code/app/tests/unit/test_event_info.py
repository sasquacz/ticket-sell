from datetime import datetime

from database import models
from tests.unit import BaseTestCase


class EventInfoTestCase(BaseTestCase):
    def setUp(self):
        super(EventInfoTestCase, self).setUp()
        ev1 = models.Event("Eventi1", datetime.utcfromtimestamp(1565128534))
        ti1 = models.Ticket("Normal", 1, 100, 10)
        self.query.save_all([ev1, ti1])

    def test_get_ok(self):
        """
            Test /event_info
            Normal scenario with success access
        """

        id_ = 1

        result = self.simulate_get(f"/event_info?id={id_}")

        expected_json = {"id": 1, "event_name": "Eventi1", "date_time": "2019-08-06 21:55:34", "tickets": [{"ticket_type": "Normal", "event_id": 1, "total": 100, "available": 100, "price": 10, "currency": "EUR"}]}

        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.json, expected_json)

    def test_get_fail_not_found_id(self):
        """
            Test /event_info
            Scenario with id which is not stored in database and app
        """

        id_ = 5

        result = self.simulate_get(f"/event_info?id={id_}")

        expected_json = {"description": "Event not found."}

        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.json, expected_json)

    def test_get_fail_bad_type_id(self):
        """
            Test /event_info
            Scenario with id of invalid type
        """

        id_ = 'z'

        result = self.simulate_get(f"/event_info?id={id_}")

        expected_json = {'title': 'Invalid parameter', 'description': 'The "id" parameter is invalid. The value must be an integer.'}

        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.json, expected_json)
