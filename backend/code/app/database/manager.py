from sqlalchemy import create_engine, orm
from sqlalchemy.exc import SQLAlchemyError

from database import models


class DBManager():
    """
        Manage connection with database
    """

    def __init__(self, connection=None):
        self.connection = connection

        self.engine = create_engine(self.connection)
        self.db_session = orm.scoping.scoped_session(
            orm.sessionmaker(
                bind=self.engine,
                autocommit=True
            )
        )

    @property
    def session(self):
        """
            Access to database
        """

        return self.db_session()

    def setup(self):
        """
            Database initializaion. Tables construction
        """

        try:
            models.SQLABase.metadata.create_all(self.engine)
            print(f"DB initialized {self.connection}")
        except SQLAlchemyError as exception:
            print(f"Could not initialize DB: {exception}")

    def drop(self):
        """
            Database initializaion. Tables construction
        """

        try:
            models.SQLABase.metadata.drop_all(self.engine)
            print(f"DB dropped {self.connection}")
        except SQLAlchemyError as exception:
            print(f"Could not drop DB: {exception}")
