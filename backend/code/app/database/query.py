from sqlalchemy import func

from database.models import Booking, Event, Ticket
from utils.enum import BookingStatus


class QueryManager():
    def __init__(self, session):
        self.session = session

    def save(self, object_):
        """
            Save object ommiting usage of session variable
        """

        with self.session.begin():
            self.session.add(object_)

    def save_all(self, object_):
        """
            Save objects ommiting usage of session variable
        """

        with self.session.begin():
            self.session.add_all(object_)

    def get_objects(self, object_):
        """
            Get all objects from database table ommiting usage of session variable
        """

        objects = []

        with self.session.begin():
            query = self.session.query(object_)
            objects = query.all()

        return objects

    def get_event(self, id_):
        """
            Get Event object by id
        """

        return self.session.query(Event).get(id_)

    def get_ticket(self, id_):
        """
            Get Ticket object by id
        """

        return self.session.query(Ticket).get(id_)

    def get_booking(self, id_):
        """
            Get Booking object by id
        """

        return self.session.query(Booking).get(id_)

    def get_booking_by_token(self, token):
        """
            Get Booking object by token field
        """

        return self.session.query(Booking).filter(Booking.token == token).one_or_none()

    def get_event_tickets(self, object_):
        """
            Get ticket objects linked with event identified by id
        """

        return self.session.query(Ticket).filter(Ticket.event_id == object_.id).all()

    def get_all_info_about_event_and_tickets(self, id_):
        """
            Gather all info about specific by id event and tickets
        """

        event = self.session.query(Event).get(id_)

        response = event.as_dict
        response['tickets'] = [ticket.as_dict for ticket in self.get_event_tickets(event)]
        return response

    def reserve_ticket(self, id_, amount):
        """
            Check availability of reservation specific amount of tickets,
            and if it's possible remove them from ticket pool.
        """

        ticket = self.session.query(Ticket).filter(Ticket.id == id_).with_for_update().one()

        if ticket.available >= amount:
            ticket.available -= amount
            self.save(ticket)
            success = True
        else:
            success = False

        return (ticket, success)

#
#   Stats section
#

    def get_stats_reserved_tickets_for_every_event(self):
        """
            Get statistics in structure
                ( Event name | Reserved tickets )
        """

        return self.session.query(
            Event.event_name, func.sum(Ticket.total) - func.sum(Ticket.available))\
            .join(Event)\
            .group_by(Ticket.event_id)\
            .group_by(Event.event_name)\
            .order_by(Ticket.event_id)\
            .all()

    def get_stats_reserved_tickets_free_proportion_for_every_event(self):
        """
            Get statistics in structure
                ( Event name | Ratio free / total )
        """

        return self.session.query(
            Event.event_name, 1.0 * func.sum(Ticket.available) / func.sum(Ticket.total))\
            .join(Event)\
            .group_by(Ticket.event_id)\
            .group_by(Event.event_name)\
            .order_by(Ticket.event_id)\
            .all()

    def get_stats_reserved_tickets_type_for_every_event(self):
        """
            Get statistics in structure
                ( Event name | Ticket type | Reserved tickets (waiting for payment & payed) )
        """

        return self.session.query(
            Event.event_name, Ticket.ticket_type, func.sum(Booking.no_tickets).filter(Booking.status != BookingStatus.cancelled))\
            .join(Ticket.bookings)\
            .join(Event)\
            .group_by(Booking.ticket_id)\
            .group_by(Ticket.id)\
            .group_by(Event.id)\
            .order_by(Booking.ticket_id)\
            .all()

    def get_stats_reserved_tickets_for_every_event_by_status(self):
        """
            Get statistics in structure
                ( Event name | Ticket type | Bookings with status waiting_for_payment | Bookings with status cancelled | Bookings with status payed )
        """

        return self.session.query(
            Event.event_name, Ticket.ticket_type, func.sum(Booking.no_tickets).filter(Booking.status == BookingStatus.waiting_for_payment), func.sum(Booking.no_tickets).filter(Booking.status == BookingStatus.cancelled), func.sum(Booking.no_tickets).filter(Booking.status == BookingStatus.payed))\
            .join(Ticket.bookings)\
            .join(Event)\
            .group_by(Booking.ticket_id)\
            .group_by(Ticket.id)\
            .group_by(Event.id)\
            .order_by(Booking.ticket_id)\
            .all()

    def get_stats_reserved_tickets_type_global(self):
        """
            Get statistics in structure
                ( Ticket type | Reserved tickets (waiting for payment & payed) )
        """

        return self.session.query(
            Ticket.ticket_type, func.sum(Booking.no_tickets).filter(Booking.status != BookingStatus.cancelled))\
            .join(Ticket)\
            .group_by(Ticket.ticket_type)\
            .order_by(Ticket.ticket_type)\
            .all()

    def get_stats_reserved_tickets_status_global(self):
        """
            Get statistics in structure
                ( Ticket type | Bookings with status waiting_for_payment | Bookings with status cancelled | Bookings with status payed )
        """

        return self.session.query(
            Ticket.ticket_type, func.sum(Booking.no_tickets).filter(Booking.status == BookingStatus.waiting_for_payment), func.sum(Booking.no_tickets).filter(Booking.status == BookingStatus.cancelled), func.sum(Booking.no_tickets).filter(Booking.status == BookingStatus.payed))\
            .join(Ticket.bookings)\
            .group_by(Ticket.ticket_type)\
            .order_by(Ticket.ticket_type)\
            .all()

    def get_stats_event_avg_payment_time(self):
        """
            Get statistics in structure
                ( Event name | Average time between reservation and payment )
        """

        return self.session.query(
            Event.event_name, func.avg(Booking.payment_time - Booking.booking_time))\
            .filter(Booking.status == BookingStatus.payed)\
            .join(Ticket.bookings)\
            .join(Event)\
            .group_by(Event.id)\
            .order_by(Event.id)\
            .all()

    def get_stats_reserved_tickets_cancel_proportion_for_every_event(self):
        """
            Get statistics in structure
                ( Event name | Ratio cancelled / all )
        """

        return self.session.query(
            Event.event_name, 1.0 * func.sum(Booking.no_tickets).filter(Booking.status == BookingStatus.cancelled) / func.sum(Booking.no_tickets))\
            .join(Ticket.bookings)\
            .join(Event)\
            .group_by(Event.id)\
            .order_by(Event.id)\
            .all()
