import datetime

from sqlalchemy import Column, Integer, String, DateTime, Enum, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from utils.enum import BookingStatus, TicketCurrency

SQLABase = declarative_base()


class Event(SQLABase):
    __tablename__ = 'events'

    id = Column(Integer, primary_key=True)
    event_name = Column(String(128), unique=True)
    date_time = Column(DateTime, default=datetime.datetime.utcnow)
    tickets = relationship("Ticket", backref="event")

    def __init__(self, event_name, date_time):
        self.event_name = event_name
        self.date_time = date_time

    @property
    def as_dict(self):
        return {
            'id': self.id,
            'event_name': self.event_name,
            'date_time': str(self.date_time)
        }

    def __repr__(self):
        return f"{self.event_name}"


class Ticket(SQLABase):
    __tablename__ = 'tickets'

    id = Column(Integer, primary_key=True)
    ticket_type = Column(String(128))
    event_id = Column(Integer, ForeignKey('events.id'))
    bookings = relationship("Booking", backref="ticket")
    total = Column(Integer)
    available = Column(Integer)
    price = Column(Integer)
    currency = Column(Enum(TicketCurrency))

    def __init__(self, ticket_type, event_id, total, price):
        self.ticket_type = ticket_type
        self.event_id = event_id
        self.total = total
        self.available = total
        self.price = price
        self.currency = TicketCurrency.EUR

    @property
    def as_dict(self):
        return {
            'ticket_type': self.ticket_type,
            'event_id': self.event_id,
            'total': self.total,
            'available': self.available,
            'price': self.price,
            'currency': self.currency.name
        }

    def __repr__(self):
        return f"{self.ticket_type}"


class Booking(SQLABase):
    __tablename__ = 'bookings'

    id = Column(Integer, primary_key=True)
    ticket_id = Column(Integer, ForeignKey('tickets.id'))
    no_tickets = Column(Integer)
    cost = Column(Integer)
    currency = Column(Enum(TicketCurrency))
    token = Column(String(128))
    status = Column(Enum(BookingStatus))
    booking_time = Column(DateTime, default=datetime.datetime.utcnow)
    payment_time = Column(DateTime)

    def __init__(self, ticket_id, no_tickets, cost):
        self.ticket_id = ticket_id
        self.no_tickets = no_tickets
        self.cost = cost

        self.currency = TicketCurrency.EUR
        self.token = ""
        self.status = BookingStatus.waiting_for_payment

    @property
    def as_dict(self):
        return {
            'id': self.id,
            'ticket_id': self.ticket_id,
            'no_tickets': self.no_tickets,
            'cost': self.cost,
            'currency': self.currency.name,
            'token': self.token,
            'status': self.status.name
        }

    def __repr__(self):
        return f"Reservation number {self.id}"
