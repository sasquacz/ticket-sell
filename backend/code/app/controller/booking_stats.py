import json

import falcon

from controller import BaseController


class BookingStatsEventReservedTickets(BaseController):
    def on_get(self, _, resp):
        """
            Get statistics in structure
                ( Event name | Reserved tickets )
        """

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = json.dumps(self.query.get_stats_reserved_tickets_for_every_event())


class BookingStatsEventFreeProportion(BaseController):
    def on_get(self, _, resp):
        """
            Get statistics in structure
                ( Event name | Ratio free / total )
        """

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = json.dumps(self.query.get_stats_reserved_tickets_free_proportion_for_every_event())


class BookingStatsEventTicketsByType(BaseController):
    def on_get(self, _, resp):
        """
            Get statistics in structure
                ( Event name | Ticket type | Reserved tickets (waiting for payment & payed) )
        """

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = json.dumps(self.query.get_stats_reserved_tickets_type_for_every_event())


class BookingStatsEventTicketsByStatus(BaseController):
    def on_get(self, _, resp):
        """
            Get statistics in structure
                ( Event name | Ticket type | Bookings with status waiting_for_payment | Bookings with status cancelled | Bookings with status payed )
        """

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = json.dumps(self.query.get_stats_reserved_tickets_for_every_event_by_status())


class BookingStatsGlobalTicketsTypes(BaseController):
    def on_get(self, _, resp):
        """
            Get statistics in structure
                ( Ticket type | Reserved tickets (waiting for payment & payed) )
        """

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = json.dumps(self.query.get_stats_reserved_tickets_type_global())


class BookingStatsGlobalTicketsStatus(BaseController):
    def on_get(self, _, resp):
        """
            Get statistics in structure
                ( Ticket type | Bookings with status waiting_for_payment | Bookings with status cancelled | Bookings with status payed )
        """

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = json.dumps(self.query.get_stats_reserved_tickets_status_global())

class BookingStatsEventAvgPaymentTime(BaseController):
    def on_get(self, _, resp):
        """
            Get statistics in structure
                ( Event name | Average time between reservation and payment )
        """

        result = self.query.get_stats_event_avg_payment_time()

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = json.dumps([[item[0], str(item[1])] for item in result])

class BookingStatsEventCancelProportion(BaseController):
    def on_get(self, _, resp):
        """
            Get statistics in structure
                ( Event name | Ratio cancelled / all )
        """

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = json.dumps(self.query.get_stats_reserved_tickets_cancel_proportion_for_every_event())
