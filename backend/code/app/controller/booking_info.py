import json

import falcon

from controller import BaseController


class BookingInfo(BaseController):
    def on_get(self, req, resp):
        """
            Gather all information about booking given by 'id' parameter
            ?id=[int]
        """

        booking_id = req.get_param_as_int('id', required=True)

        booking = self.query.get_booking(booking_id)
        if not booking:
            resp.status = falcon.HTTP_400 #pylint: disable=E1101
            resp.body = json.dumps({'description': "Booking not found."})
            return

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = json.dumps(booking.as_dict)
