import json

import falcon

from controller import BaseController


class EventInfo(BaseController):
    def on_get(self, req, resp):
        """
            Gather all information about event given by 'id' parameter
            ?id=[int]
        """

        event_id = req.get_param_as_int('id', required=True)

        event = self.query.get_event(event_id)
        if not event:
            resp.status = falcon.HTTP_400 #pylint: disable=E1101
            resp.body = json.dumps({'description': "Event not found."})
            return

        event_info = self.query.get_all_info_about_event_and_tickets(event_id)

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = json.dumps(event_info)
