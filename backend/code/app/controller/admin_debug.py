import datetime
import random

import falcon

from controller import BaseController
from database import models


class AddEvent(BaseController):
    def on_get(self, _, resp):
        """
            Add one event with random number into DB
        """

        event_number = random.randint(9, 9999999)
        event = models.Event(
            event_name=f"Event_{event_number}",
            date_time=datetime.datetime.utcnow()
        )
        self.save(event)

        objects_to_save = []
        objects_to_save.append(models.Ticket(
            ticket_type='Normal',
            event_id=event.id,
            total=100,
            price=50
        ))

        objects_to_save.append(models.Ticket(
            ticket_type='VIP',
            event_id=event.id,
            total=10,
            price=100
        ))

        self.save_all(objects_to_save)

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = "1 event added"


class AddEvents(BaseController):
    def on_get(self, _, resp):
        """
        Add 20 events with random number into DB
        """

        self.db_.drop()
        self.db_.setup()

        objects_to_save = []
        for _ in range(20):
            event_number = random.randint(9, 9999999)
            event = models.Event(
                event_name=f"Event_{event_number}",
                date_time=datetime.datetime.utcnow()
            )
            self.save(event)

            normal_ticket_names = ["Normal", "Regular", "A Class"]
            objects_to_save.append(models.Ticket(
                ticket_type=random.choice(normal_ticket_names),
                event_id=event.id,
                total=random.randint(20, 500),
                price=random.randint(2, 100)
            ))

            vip_ticket_names = ["VIP", "Premium", "Premium Class"]
            objects_to_save.append(models.Ticket(
                ticket_type=random.choice(vip_ticket_names),
                event_id=event.id,
                total=random.randint(2, 100),
                price=random.randint(20, 500)
            ))

        self.save_all(objects_to_save)

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = "20 events added"
