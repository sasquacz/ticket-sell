from database.query import QueryManager

class BaseController():
    def __init__(self, db_manager):
        self.db_ = db_manager
        self.query = QueryManager(self.db_.session)

    def save(self, object_):
        """
            Save given object to DB
        """
        self.query.save(object_)

    def save_all(self, object_):
        """
            Save given objects to DB
        """
        self.query.save_all(object_)
