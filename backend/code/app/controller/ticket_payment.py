

import json

import celery
import falcon

import controller.adapters.payment_gateway as PG
from controller import BaseController
from utils.database import REDIS_URL
from utils.enum import BookingStatus


class TicketPayment(BaseController):
    def on_get(self, req, resp):
        """
            Complete payment for booking given by uuid4 token
            ?token=[uuid]
        """

        token = req.get_param_as_uuid('token', required=True)

        booking = self.query.get_booking_by_token(str(token))

        if not booking:
            resp.status = falcon.HTTP_400 #pylint: disable=E1101
            resp.body = json.dumps({'description': "Booking not found."})
            return

        if booking.status is not BookingStatus.waiting_for_payment:
            resp.status = falcon.HTTP_408 #pylint: disable=E1101
            resp.body = json.dumps({'description': 'Timeout, booking canceled, please reserve ticket again and complete payment in 15 minutes. Or Your payment completed earlier.'})
            return

        try:
            PG.PaymentGateway().charge(booking.cost, booking.token, booking.currency.name)
        except PG.CardError as exception:
            resp.status = falcon.HTTP_406 #pylint: disable=E1101
            resp.body = json.dumps({'description': str(exception)})
            return
        except (PG.CurrencyError, PG.PaymentError) as exception:
            resp.status = falcon.HTTP_500 #pylint: disable=E1101
            resp.body = json.dumps({'description': str(exception)})
            return

        with celery.Celery("booking", broker=REDIS_URL) as queue:
            queue.control.revoke(token, terminate=True)
            queue.send_task('booking.booking_confirmer', args=(booking.id,))

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = json.dumps({'description': 'Payment complete.'})
