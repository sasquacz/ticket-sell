import json

import celery
import falcon

from controller import BaseController
from database import models
from utils.database import REDIS_URL


class TicketBooking(BaseController):
    def on_get(self, req, resp):
        """
            Book tickets given by 'id' and 'no_tickets' for event for 15 minutes
            ?id=[int]&no_ticket=[int]
        """

        ticket_id = req.get_param_as_int('id', required=True)
        no_tickets = req.get_param_as_int('no_tickets', required=True)

        if no_tickets < 1:
            resp.status = falcon.HTTP_400 #pylint: disable=E1101
            resp.body = json.dumps({'description': "Cannot buy less than one ticket."})
            return

        ticket = self.query.get_ticket(ticket_id)
        if not ticket:
            resp.status = falcon.HTTP_400 #pylint: disable=E1101
            resp.body = json.dumps({'description': "Tickets for this event not found."})
            return

        ticket, status = self.query.reserve_ticket(ticket_id, no_tickets)

        if status:
            total_cost = ticket.price*no_tickets
            booking = models.Booking(ticket_id, no_tickets, total_cost)
            self.save(booking)

            with celery.Celery("booking", broker=REDIS_URL) as queue:
                task_id = queue.send_task('booking.booking_canceler', args=(booking.id, ticket_id, no_tickets), countdown=900)

            booking.token = str(task_id)
            self.save(booking)

            response = {
                'event_id': ticket.event_id,
                'ticket_id': ticket_id,
                'no_tickets': no_tickets,
                'total_cost': total_cost,
                'payment_token': str(booking.token),
                'status': booking.status.name
                }

            resp.status = falcon.HTTP_200 #pylint: disable=E1101
            resp.body = json.dumps(response)

        else:
            resp.status = falcon.HTTP_409 #pylint: disable=E1101
            resp.body = json.dumps({'description': f"Cannot buy {no_tickets} tickets, but still is available {ticket.available}"})
