import json

import falcon

from controller import BaseController


class EventTickets(BaseController):
    def on_get(self, req, resp):
        """
            Gather information about tickets for event given by 'id' parameter
            ?id=[int]
        """
        event_id = req.get_param_as_int('id', required=True)

        event = self.query.get_event(event_id)

        if not event:
            resp.status = falcon.HTTP_400 #pylint: disable=E1101
            resp.body = json.dumps({'description': "Event not found."})
            return
        event_info = self.query.get_all_info_about_event_and_tickets(event_id)

        tickets = []
        for ticket in event_info['tickets']:
            ticket['can_buy'] = ticket['available'] > 0
            tickets.append(ticket)

        resp.status = falcon.HTTP_200 #pylint: disable=E1101
        resp.body = json.dumps(tickets)
