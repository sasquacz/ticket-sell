from collections import namedtuple


class CardError(Exception):
    pass


class PaymentError(Exception):
    pass


class CurrencyError(Exception):
    pass


PaymentResult = namedtuple('PaymentResult', ('amount', 'currency'))


class PaymentGateway: #pylint: disable=R0903
    supported_currencies = ('EUR',)

    def charge(self, amount, token, currency='EUR'):
        if token == '042800b6-9759-4235-a8f2-592d4ab0c2a1':
            raise CardError("Your card has been declined")
        if token == '2b88c420-3cb1-4df0-ba92-60d000217480':
            raise PaymentError("Something went wrong with your transaction")
        if currency not in self.supported_currencies:
            raise CurrencyError(f"Currency {currency} not supported")

        return PaymentResult(amount, currency)
