import falcon

from controller import (admin_debug, booking_info, booking_stats, event_info,
                        event_tickets, ticket_booking, ticket_payment)
from database.manager import DBManager
from utils.database import POSTGRES_URL


def initialize_db(database_url):
    """
        Create instance of db manager which is needed by create_app(db_manager)
    """

    manager = DBManager(database_url)
    manager.setup()
    return manager

def create_app(db_manager):
    """
        Create app instance with all api endpoints
    """
    api = falcon.API()

    #
    #    Main functionality
    #
    api.add_route('/event_info', event_info.EventInfo(db_manager))
    api.add_route('/event_tickets', event_tickets.EventTickets(db_manager))
    api.add_route('/tickets_booking', ticket_booking.TicketBooking(db_manager))
    api.add_route('/tickets_payment', ticket_payment.TicketPayment(db_manager))
    api.add_route('/booking_info', booking_info.BookingInfo(db_manager))

    #
    #    Booking stats
    #
    api.add_route('/stats/event_reserved', booking_stats.BookingStatsEventReservedTickets(db_manager))
    api.add_route('/stats/event_free_proportion', booking_stats.BookingStatsEventFreeProportion(db_manager))
    api.add_route('/stats/event_tickets_type', booking_stats.BookingStatsEventTicketsByType(db_manager))
    api.add_route('/stats/event_tickets_status', booking_stats.BookingStatsEventTicketsByStatus(db_manager))
    api.add_route('/stats/global_tickets_type', booking_stats.BookingStatsGlobalTicketsTypes(db_manager))
    api.add_route('/stats/global_tickets_status', booking_stats.BookingStatsGlobalTicketsStatus(db_manager))
    api.add_route('/stats/event_avg_time', booking_stats.BookingStatsEventAvgPaymentTime(db_manager))
    api.add_route('/stats/event_cancel_proportion', booking_stats.BookingStatsEventCancelProportion(db_manager))

    #
    #    Development debug
    #
    api.add_route('/add_event', admin_debug.AddEvent(db_manager))
    api.add_route('/add_events', admin_debug.AddEvents(db_manager))

    return api

def run_app():
    """
        Function called by gunicorn
    """
    database_manager = initialize_db(POSTGRES_URL)
    return create_app(database_manager)
