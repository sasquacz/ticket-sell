[![pipeline status](https://gitlab.com/sasquacz/ticket-sell/badges/master/pipeline.svg)](https://gitlab.com/sasquacz/ticket-sell/commits/master)
# TicketSeller

Ticket selling platform, without frontend part.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- docker 18.09.2+
- docker-compose 1.23.2+

### Installing

Firstly you need to clone this repository.
After that you need to rename env-example into .env file. You can modify variable inside.

Last step to run app is get in main directory of project and run docker-compose.

> docker-compose up --build

### Running the tests

For tests (pylint and unit tests) run, you need get in main directory and run test version of docker-compose.

> docker-compose -f docker-compose-tests.yml up --build

## API

### Main functionality
#### GET /event_info?id=[int]
##### Input:
> id - integer - describing event what you request

##### Output OK:
> status code: 200
body: {"id": [int], "event_name": [str], "date_time": [datetime in utc format], "tickets": [{"ticket_type": [str], "event_id": [int], "total": [int], "available": [int], "price": [int], "currency": [enum:TicketCurrency]} ... ]}

##### Output FAIL:
> status code: 400
body: {'title': 'Invalid parameter', 'description': 'The "id" parameter is invalid. The value must be an integer.'}

When you give invalid type of parameter.

> status code: 400
body: {"description": "Event not found."}

When you give value which is not stored in database and app.

#### GET /event_tickets?id=[int]
##### Input:
> id - integer - describing event what you request

##### Output OK:
> status code: 200
body: [{'ticket_type': [str], 'event_id': [int], 'total': [int], 'available': [int], 'price': [int], 'currency': [enum:TicketCurrency], 'can_buy': [bool] ... ]

##### Output FAIL:
> status code: 400
body: {'title': 'Invalid parameter', 'description': 'The "id" parameter is invalid. The value must be an integer.'}

When you give invalid type of parameter.

> status code: 400
body: {"description": "Event not found."}

When you give value which is not stored in database and app.

#### GET /tickets_booking?id=[int]&no_tickets=[int]
##### Input:
> id - integer - describing ticket what you request booking
no_tickets - integer - describing number of tickets you request booking

##### Output OK:
> status code: 200
body: {'event_id': [int], 'ticket_id': [int], 'no_tickets': [int], 'total_cost': [int], 'payment_token': [uuid], 'status': [enum:BookingStatus]}

##### Output FAIL:
> status code: 400
body: {'title': 'Invalid parameter', 'description': 'The "id" parameter is invalid. The value must be an integer.'}

When you give invalid type of id parameter.

> status code: 400
body: {'title': 'Invalid parameter', 'description': 'The "no_tickets" parameter is invalid. The value must be an integer.'}

When you give invalid type of no_tickets parameter.

> status code: 400
body: {'description': "Cannot buy less than one ticket."}

When no_tickets is less than 0.

> status code: 400
body: {'description': "Tickets for this event not found."}

When you give id value which is not stored in database and app.

> status code: 409
body: {'description': "Cannot buy X tickets, but still is available Y"}

When you want buy X tickets but for event is available only Y tickets. (X > Y)

#### GET /tickets_payment?token=[uuid]
##### Input:
> token - uuid (any format) - describing reservation for you want pay

##### Output OK:
> status code: 200
body: {'description': 'Payment complete.'}

##### Output FAIL:
> status code: 400
body: {'description': 'The "token" parameter is invalid. The value must be a UUID string.', 'title': 'Invalid parameter'}

When you give invalid type of parameter.

> status code: 400
body: {"description": "Booking not found."}

When you give value which is not stored in database and app.

> status code: 406
body: {'description': 'Your card has been declined'}

When your card is declined, please use another card.

> status code: 408
body: {'description': 'Timeout, booking canceled, please reserve ticket again and complete payment in 15 minutes. Or Your payment completed earlier.'}

When you trying pay for reservation with status enum:BookingStatus cancelled / payed.

> status code: 500
body: {'description': 'Something went wrong with your transaction'}

When our provider have problem with your payment. Please try again later.

> status code: 500
body: {'description': 'Currency USD not supported'}

When you try to pay for reservation in not supported currency.

#### GET /booking_info?id=[int]
##### Input:
> id - integer - describing reservation what you request

##### Output OK:
> status code: 200
body: {'cost': [int], 'currency': [enum:TicketCurrency], 'id': [int], 'no_tickets': [int], 'status': [enum:BookingStatus], 'ticket_id': [int], 'token': [uuid]}


##### Output FAIL:
> status code: 400
body: {'title': 'Invalid parameter', 'description': 'The "id" parameter is invalid. The value must be an integer.'}

When you give invalid type of parameter.

> status code: 400
body: {"description": "Booking not found."}

When you give value which is not stored in database and app.

### Booking stats
Every stats request can return empty list as output ([]) when app and database are empty. Otherwise it return list with list in specific structure as described below.
#### GET /stats/event_reserved
##### Output structure
> ( Event_name [str], Reserved_tickets [int] )

#### GET /stats/event_free_proportion
##### Output structure
> ( Event_name [str], Ratio_free_/_total [float] )

#### GET /stats/event_tickets_type
##### Output structure
> ( Event_name [str], Ticket_type [str], Reserved_tickets_(waiting_for_payment_&_payed) [int] )

#### GET /stats/event_tickets_status
##### Output structure
> ( Event_name [str], Ticket_type [str], Bookings_with_status_waiting_for_payment [int],  Bookings_with_status_cancelled [int], Bookings_with_status_payed [int] ) 

#### GET /stats/global_tickets_type
##### Output structure
>  ( Ticket_type [str], Reserved_tickets_(waiting_for_payment_&_payed) [int] )


#### GET /stats/global_tickets_status
##### Output structure
> ( Ticket_type [str], Bookings_with_status_waiting_for_payment [int],  Bookings_with_status_cancelled [int], Bookings_with_status_payed [int] )


#### GET /stats/event_avg_time
##### Output structure
> ( Event_name [str], Average_time_between_reservation_and_payment [datetime.timedelta] )

#### GET /stats/event_cancel_proportion
##### Output structure
>  ( Event_name [str], Ratio_cancelled_/_all [float] )

### Dev endpoints, to remove in production version
#### GET /add_event
Is adding one event with two tickets into database.
#### GET /add_events
Is adding 20 events with two tickets for each into database.

### Enums stored in app:
#### TicketCurrency
> EUR = 1
USD = 2

#### BookingStatus
> waiting_for_payment = 1
cancelled = 2
payed = 3