import datetime
import os

from celery import Celery, task
from psycopg2 import DatabaseError, connect

app = Celery("booking", broker='redis://redis')

@task(bind=True)
def booking_canceler(self, booking_id, ticket_id, no_tickets):
    conn = None
    try:
        conn = connect(f"dbname={os.environ['POSTGRES_DB']} user={os.environ['POSTGRES_USER']} password={os.environ['POSTGRES_PASSWORD']} host=postgres port=5432")
        cur = conn.cursor()
        cur.execute(f"SELECT available FROM tickets WHERE id={ticket_id}")
        available = cur.fetchone()[0]
        cur.execute(f"UPDATE tickets SET available = {available + no_tickets} WHERE id={ticket_id}")
        cur.execute(f"UPDATE bookings SET status = 'cancelled' WHERE id={booking_id}")
        conn.commit()
        conn.close()
    except (Exception, DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

@task(bind=True)
def booking_confirmer(self, booking_id):
    conn = None
    try:
        conn = connect(f"dbname={os.environ['POSTGRES_DB']} user={os.environ['POSTGRES_USER']} password={os.environ['POSTGRES_PASSWORD']} host=postgres port=5432")
        cur = conn.cursor()
        cur.execute(f"UPDATE bookings SET status = 'payed', payment_time = '{datetime.datetime.utcnow()}' WHERE id={booking_id}")
        conn.commit()
        conn.close()
    except (Exception, DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
